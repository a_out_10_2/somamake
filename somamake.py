#!/usr/bin/env python3
# -----------------------------------------------------------------------------
#   somamake
#
#   Copyright (C) 2015  Andrew Moe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. Or, see
#   <http://www.gnu.org/licenses/gpl-2.0.html>.
# -----------------------------------------------------------------------------
import sys
import argparse
import logging
import os.path
import re

import urllib.request
import xml.etree.ElementTree

import xspf

__version__ = 0.1

# Constants
playlist_title = "Soma.FM Playlist"
playlist_info = "http://somafm.com/"
playlist_creator = "datalusd"
track_creator = "Soma.FM"
sfm_license = """Copyright ©2000-2018 SomaFM.com, LLC. All rights reserved. Groove Salad, Drone Zone, Cliqhop, 
                 Secret Agent, Space Station Soma, Digitalis, PopTron, Suburbs of Goa and Illinois Street Lounge 
                 are trademarks of SomaFM.com, LLC."""

TMPDIR = os.path.join(os.getcwd(), "tmp")
MP3 = "mp3"
AAC = "aac"
AACPLUS = "aacp"
SLOW = "slowpls"
FAST = "fastpls"
HIGHEST = "highestpls"

re_location_url = re.compile("^[f][i][l][e][0-9]+=(.*)", re.IGNORECASE)


def parse_args(*args):
    """
    Parse the arguments received from STDIN.
    :param args: The string arguments to be parsed.
    :return params: The arguments parsed into parameters.
    :rtype: argparse.Namespace
    """
    # Constructing argument parser
    parser = argparse.ArgumentParser(description="A robust script for generating a master playlist for "
                                                 "Soma.fm stations.")
    parser.add_argument("-t", "--target-url", default="http://api.somafm.com/channels.xml",
                        help="The target URL from which to download SomaFM's channel data.")
    parser.add_argument("-c", "--cache-data", action='store_true', default=True,
                        help="Cache retrieved channel data to a temporary directory.")
    codec_group = parser.add_mutually_exclusive_group()
    codec_group.add_argument("-b", "--use-allcodecs", action='store_true', required=False, default=True,
                             help="Use all available streams in mirror list, regardless of format.")
    codec_group.add_argument("-a", "--use-aac", action='store_true', required=False, default=False,
                             help="Use the AAC mirrors when generating a playlist.")
    codec_group.add_argument("-m", "--use-mp3", action='store_true', required=False, default=False,
                             help="Use the MP3 mirrors when generating a playlist.")
    quality_group = parser.add_mutually_exclusive_group()
    quality_group.add_argument("-g", "--prefer-hifi", action='store_true', required=False, default=True,
                               help="Use high quality streams and default to lower quality subsequent mirrors.")
    quality_group.add_argument("-p", "--prefer-lofi", action='store_true', required=False, default=False,
                               help="Use low quality streams and default to higher quality subsequent mirrors. "
                                    "(Useful when data rationing on Nazi ISPs.)")
    parser.add_argument("-v", "--verbose", action="count", default=0, help="Display debug output during runtime.")
    parser.add_argument("--version", action='version', version='somamake %s' % __version__)

    return parser.parse_args(*args)


def request_channel_data(url, filename=None):
    """
    Retrieve the SomaFM channel data.
    :param url: Uniform Resource Locator (URL) that points to the SomaFM channel listings.
    :param filename: If defined, cache received data to $CWD/tmp/filename .
    :return: The channel listings data.
    :rtype: bytes
    """
    logging.debug("Requesting data from " + url)
    response = None
    try:
        response = urllib.request.urlopen(url)

    except ValueError as ve:
        logging.critical("Exception while parsing target_source \"" + url + "\".")
        logging.critical(str(type(ve).__name__) + ": " + str(ve))
        sys.exit(1)

    except urllib.request.URLError as urle:
        logging.critical("Exception when requesting station data.")
        logging.critical(str(type(urle).__name__) + ": " + str(urle))
        sys.exit(1)

    except:
        logging.critical("An unexpected error occurred when requesting data.")
        raise

    logging.debug("Successfully received data!")

    data = response.read()

    # Write channel data to file if filename is defined
    if filename is not None:
        with open(filename, 'wb') as outfile:
            outfile.write(data)

    return data


def main(params):
    """
    Execute the main method of the program.
    :param params: The parameters that will dictate the functionality of the program.
    :return: The final return code of the program.
    :rtype: int
    """
    # Set up logging
    if params.verbose == 1:
        logging.basicConfig(format='%(message)s', level=logging.INFO)
    elif params.verbose >= 2:
        logging.basicConfig(format='%(message)s', level=logging.DEBUG)

    # Display program variables
    logging.debug("somamake input arguments")
    logging.debug("\t--target-url\t{}".format(params.target_url))
    logging.debug("\t--cache-data\t{}".format(params.cache_data))
    logging.debug("\t--use-allcodecs\t{}".format(params.use_allcodecs))
    logging.debug("\t--use-aac\t{}".format(params.use_aac))
    logging.debug("\t--use-mp3\t{}".format(params.use_mp3))
    logging.debug("\t--prefer-hifi\t{}".format(params.prefer_hifi))
    logging.debug("\t--prefer-lofi\t{}".format(params.prefer_lofi))
    logging.debug("\t--verbose\t{}".format(params.verbose))

    # Initialize temporary directory, if desired
    if params.cache_data:
        try:
            os.mkdir(TMPDIR)
        except FileExistsError:
            pass

    # Initialize the XSPF encoder
    playlist = xspf.Xspf()
    playlist.title = playlist_title
    playlist.info = playlist_info
    playlist.creator = playlist_creator
    playlist.license = sfm_license

    # Attempt to acquire the Soma.fm channel list from target_url address
    if params.cache_data:
        pagedata = request_channel_data(params.target_url,
                                        filename=os.path.join(TMPDIR, os.path.split(params.target_url)[1]))
    else:
        pagedata = request_channel_data(params.target_url)

    # Prioritize desired streaming rates
    bitrates = []
    if params.prefer_hifi:
        bitrates = [HIGHEST, FAST]
    elif params.prefer_lofi:
        bitrates = [SLOW]
    else:
        logging.warning("Desired stream bitrate is unknown. Using all rates.")
        bitrates = [HIGHEST, FAST, SLOW]

    # Define acceptable location formats
    formats = []
    if params.use_allcodecs:
        formats = [AACPLUS, AAC, MP3]
    elif params.use_aac:
        formats = [AACPLUS, AAC]
    elif params.use_mp3:
        formats = [MP3]
    else:
        logging.warning("Desired stream format is unknown. Using all known formats.")
        formats = [AACPLUS, AAC, MP3]

    # Parse channel data from XML file
    channeltree = xml.etree.ElementTree.fromstring(pagedata)
    for channel in channeltree:

        logging.info("Processing \"" + channel.attrib["id"] + "\"")

        # Define the XSPF track for this channel
        track = xspf.Track()
        track.creator = track_creator

        # URL locations for each selected stream
        locations = []

        # For every SomaFM channel, define a track.
        for channel_element in channel:

            # The channel title is the track title.
            if channel_element.tag == "title":
                track.title = channel_element.text
                logging.debug("Parsed title:\t\t\t" + track.title)
            # The channel image is the track image.
            elif channel_element.tag == "image":
                track.image = channel_element.text
                logging.debug("Parsed image location:\t" + track.image)
            # The channel description is the track annotation.
            elif channel_element.tag == "description":
                track.annotation = channel_element.text
                logging.debug("Parsed description:\t\t" + track.annotation)

            # The channel's pls elements define .pls playlist files that contain the URL locations of the stream.
            # elif channel_element.tag == "highestpls" or channel_element.tag == "fastpls":
            elif str(channel_element.tag) in bitrates and channel_element.get("format") in formats:

                # Request webpage from URL
                data = request_channel_data(channel_element.text)

                # Cache .pls file, if desired
                if params.cache_data:
                    with open(os.path.join(TMPDIR, os.path.split(channel_element.text)[1]), 'wb') as outfile:
                        outfile.write(data)

                # Extract each location from the PLS file
                for line in str(data).split("\\n"):
                    match_location_url = re_location_url.match(line)
                    if match_location_url:
                        locations.append(match_location_url.group(1))

        # Set track locations
        track.location = locations
        playlist.add_track(track)
        logging.info("")

    # Write the playlist to file
    with open("somafm.xspf", 'wb') as outfile:
        outfile.write(playlist.toXml())

    return 0


# Program execution start
if __name__ == "__main__":
    params = parse_args()
    sys.exit(main(params))
